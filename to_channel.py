""" This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2019 - Davide Leone """


from CONFIG import *

def send():

    """This function is used to send one of the media to the channel.
    The bot will select one of them that has to be send and, if
    find no one, send an alert to the main admin."""

    from CONFIG import bot, channel, telepot, database, main_admin

    archive = json.load(open(database))
    media = list(archive.keys())
    send_one_time = 0 
    

    for file_id in media:

        if archive[file_id]['status'] == 0 and send_one_time == 0:
            message = ''
            send_one_time = 1

            if archive[file_id]['type'] == 'photo':

                bot.sendPhoto(
                    chat_id = channel,
                    photo = file_id,
                    caption = getCaption(),
                    parse_mode = 'HTML'
                    )

                archive[file_id]['status'] = 1

            elif archive[file_id]['type'] == 'video':
                
                bot.sendVideo(
                    chat_id = channel,
                    video = file_id,
                    caption = getCaption(),
                    parse_mode = 'HTML'
                    )


            elif archive[file_id]['type'] == 'document':

                bot.sendDocument(
                    chat_id = channel,
                    document = file_id,
                    caption = getCaption(),
                    parse_mode = 'HTML'
                    )
                

            with open(database,'w') as outfile:
                    json.dump(archive,outfile,indent=4)


    if send_one_time == 0:
        message = "I'm sorry, but I haven't any media to send to the channel."

        bot.sendMessage(
            chat_id = main_admin,
            text = message
            )

def getCaption():

    """This function is used to send create the caption for the media.
    The caption is composed by the title of the channel, formatted with
    it's link. So, the bot will get the link or generate a new one, then
    use HTML to create the caption."""

    from CONFIG import bot, channel, telepot
    
    chat = bot.getChat(channel)

    if 'invite_link' in list(chat.keys()):
        link = chat['invite_link']

    else:
        link = bot.exportChatInviteLink(channel)

    name = html.escape(chat['title'],True)

    caption = '<a href="'+link+'">'+name+'</a>'

    return caption

    
        
            
    
