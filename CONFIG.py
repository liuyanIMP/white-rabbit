""" Configuration file for Meme Channel Admninistrator Bot.
    Make sure to have modified this before running the bot.
    
    Copyright (C) 2019  Davide Leone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#-- IMPORT --
import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from telepot.loop import MessageLoop
import time
import json
import html
import pickle
import datetime

#-- SETTINGS --
token = ' ' #This is the bot's token. To get here, use t.me/botfather
database = ' ' #.json file that include all the media
database_staff = ' ' #A file that stores the staff's information
developer = 't.me/davideleone' #Telegram link to contact the developer
source_link = 'https://gitlab.com/tea-project/white-rabbit/' #Link to the source code
channel = -0 #The channel where the bot will send all the media
main_admin = 0 #ID of the main admin
hours = [8,14,18,20] #Write the numbers, in int type, that rappresents each hour you want the media to be sent

#-- SET UP THE CONNECTION USING THE TOKEN --
bot = telepot.Bot(token) 

#-- CREATE THE DATABASES FILES --
try:
    open(database)
except FileNotFoundError:
    with open(database,'w') as outfile:
                json.dump({},outfile,indent=4)

try:
    open(database_staff)
except FileNotFoundError:
    with open(database_staff,'wb') as outfile:
        pickle.dump([],outfile)
