""" This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2019 - Davide Leone """


from CONFIG import *

def old_media(days):

    """This function is used to get a list with all the files that
    have status zero (not already sent) and have been quequed a
    certain amount (days) ago."""

    from CONFIG import bot, telepot, database

    archive = json.load(open(database))
    media = list(archive.keys())

    files = []
    current_time = datetime.datetime.today()

    for file_id in media:


        if archive[file_id]['status'] == 0 :
            file_time = datetime.datetime.fromtimestamp(archive[file_id]['time'])
            difference = abs((file_time - current_time).days)

            if difference > days:
                files.append(file_id)

    return files

def delete_old_media(days):

    """This function let you delete (set the status to two) the media
    older than a certain amount of days. It does select the media using
    the old_media function."""

    from CONFIG import bot, channel, telepot

    archive = json.load(open(database))

    files = old_media(days) 

    for file_id in files:
        archive[file_id]['status'] = 2
    
        
    with open(database,'w') as outfile:
        #Update the database
        json.dump(archive,outfile,indent=4)

    return len(files)
            
    
